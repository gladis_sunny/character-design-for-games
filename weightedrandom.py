import random
def weighted_word_selection(words, weights):
    """ 
    words : an array of strings (words) 
    weights : an array of floats (corresponding probabilties based on)
              index number.
    """
    start = 0
    for i in range(len(weights)):
        weights[i] = start+weights[i]
        start += weights[i]
    r = random.uniform(0, 1.0)
    for i in range(len(weights)):
        if r < weights[i]:
            return words[i]
